﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hafele_Shagun.Models
{
    public class Login
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Emailid { get; set; }
        public string Mobileno { get; set; }
        public string Remark { get; set; }
        public string UserType { get; set; }
        public string OldPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}