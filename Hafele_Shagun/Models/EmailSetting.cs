﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hafele_Shagun.Models
{
    public class EmailSetting
    {
        public string ServerName { get; set; }
        public string Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool UserDefaultCredentials { get; set; }
        public bool EnableSSL { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string Body { get; set; }
        public bool IsBodyHtml { get; set; }
        public bool WriteEmailToDisk { get; set; }
    }
}