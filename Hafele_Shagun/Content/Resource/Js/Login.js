﻿

function ValidateFields() {
    debugger;
    $(".imgloader").show();

    var username = $('#txtusername').val();
    var password = $('#txtpassword').val();
    var usertype = $('#UserType').val();
    var blank = true;

    if (username != '' && password != '') {

        blank = false;
    }
    else {

        blank = true
    }
    if (blank != true) {

        var Login = {}; //Creating Class List
        Login.Username = username;
        Login.Password = password;
        Login.UserType = usertype;


        $.ajax({
            type: "POST",
            url: "/Login/LogProcess",
            data: JSON.stringify(Login),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.Responsetype == "Success") {
                    location.href = data.Url;
                }
                else {
                    $('#validationmessage').attr('style', 'display:block');
                    $('#lblmsg').attr('style', 'color:red');
                    $('#lblmsg').html('Invalid Username and Password!');

                    $(".imgloader").hide();
                    $("#btnlogin").show();
                }
            }
        });
        return true;
    }
    else {
        //alert('wrong');
        $('#validationmessage').attr('style', 'display:block');
        $('#lblmsg').attr('style', 'color:red');
        $('#lblmsg').html('Please Enter Username and Password!');

        $(".imgloader").hide();
        return false;
    }
};


$(document).ready(function () {
    $('.clearoldmsg').on('click', function (e) {

        $('#validationmessage').attr('style', 'display:none');
        $('#lblmsg').attr('style', '');
        $('#lblmsg').html('');

    });

});





function ForgotValidateFields() {
    $(".imgloader").show();
    var username = $('#txtforgotemail').val();
    var blank = true;

    if (username != '') {

        blank = false;
    }
    else {

        blank = true
    }
    if (blank != true) {
        // alert('right');
        var usertype = $('#UserType').val();

        var Login = {}; //Creating Class List
        Login.Emailid = username;
        Login.UserType = usertype;


        $.ajax({
            type: "POST",
            url: "/Login/ForgotPassword",
            data: JSON.stringify(Login),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $(".imgloader").hide();
                if (data.Responsetype == "Success") {
                    $('#txtforgotemail').val('');
                    $('#validationmessage1').attr('style', 'display:block');
                    $('#lblmsg1').attr('style', 'color:green');
                    $('#lblmsg1').html('Password sent to your email address.');

                }
                else {
                    $('#validationmessage1').attr('style', 'display:block');
                    $('#lblmsg1').attr('style', 'color:red');
                    $('#lblmsg1').html('Invalid email address.');

                }
            }
        });
    }
    else {
        $(".imgloader").hide();
        $('#validationmessage1').attr('style', 'display:block');
        $('#lblmsg1').attr('style', 'color:red');
        $('#lblmsg1').html('Please enter email address.');
        return false;
    }
};



$(document).keypress(function (e) {

    if (e.which == 13) {
        var url = window.location.hash;
        if (url == '#signup') {
            alert('Enter button is a disable please click button!');
            return false;
            // $("#btnforgot").click();
        }
        else {
            $("#btnlogin").click();
        }

    }
});


$(function () {
    $('#txtusername21').keydown(function (e) {
        if (e.shiftKey || e.ctrlKey || e.altKey) {
            e.preventDefault();
        } else {
            var key = e.keyCode;
            if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                e.preventDefault();
            }
        }
    });
});
