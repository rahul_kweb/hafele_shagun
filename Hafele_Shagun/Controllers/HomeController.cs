﻿using Hafele_Shagun.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Hafele_Shagun.Controllers
{
    public class HomeController : Controller
    {
        Utility util = new Utility();
        // GET: Home
        public ActionResult Index(string utm_source, string utm_medium, string utm_content, string utm_campaign, string Device, string City)
        {
            // Add Product in Checkbox list

            List<SelectListItem> items = new List<SelectListItem>()
            {
                 new SelectListItem {Value="Combo Offer 1", Text = "Combo Offer 1",  Selected= false},
                 new SelectListItem{Value="Combo Offer 2", Text = "Combo Offer 2", Selected = false},
                 new SelectListItem{Value="Combo Offer 3", Text = "Combo Offer 3", Selected = false},
                 new SelectListItem{Value="Combo Offer 4", Text = "Combo Offer 4", Selected = false},
                 new SelectListItem{Value="Combo Offer 5", Text = "Combo Offer 5", Selected = false},
                 new SelectListItem{Value="Combo Offer 6", Text = "Combo Offer 6", Selected = false},
                 new SelectListItem{Value="Combo Offer 7", Text = "Combo Offer 7", Selected = false},
                 new SelectListItem{Value="Combo Offer 8", Text = "Combo Offer 8", Selected = false},
                 new SelectListItem{Value="Combo Offer 9", Text = "Combo Offer 9", Selected = false},
                 new SelectListItem{Value="Combo Offer 10", Text = "Combo Offer 10", Selected = false},
                 new SelectListItem{Value="Combo Offer 11", Text = "Combo Offer 11", Selected = false},
                 new SelectListItem{Value="Combo Offer 12", Text = "Combo Offer 12", Selected = false},
                 new SelectListItem{Value="Combo Offer 13", Text = "Combo Offer 13", Selected = false},
                 new SelectListItem{Value="Combo Offer 14", Text = "Combo Offer 14", Selected = false},
                 new SelectListItem{Value="Combo Offer 15", Text = "Combo Offer 15", Selected = false},
                 new SelectListItem{Value="Combo Offer 16", Text = "Combo Offer 16", Selected = false},
                 new SelectListItem{Value="Combo Offer 17", Text = "Combo Offer 17", Selected = false},
                 new SelectListItem{Value="Combo Offer 18", Text = "Combo Offer 18", Selected = false},
                 new SelectListItem{Value="Combo Offer 19", Text = "Combo Offer 19", Selected = false},
                 new SelectListItem{Value="Combo Offer 20", Text = "Combo Offer 20", Selected = false},
                 new SelectListItem{Value="Combo Offer 21", Text = "Combo Offer 21", Selected = false},
                 new SelectListItem{Value="Combo Offer 22", Text = "Combo Offer 22", Selected = false},
                 new SelectListItem{Value="Combo Offer 23", Text = "Combo Offer 23", Selected = false},
                 new SelectListItem{Value="Combo Offer 24", Text = "Combo Offer 24", Selected = false},
                 new SelectListItem{Value="Combo Offer 25", Text = "Combo Offer 25", Selected = false},
                 new SelectListItem{Value="Combo Offer 26", Text = "Combo Offer 26", Selected = false},
                 new SelectListItem{Value="Combo Offer 27", Text = "Combo Offer 27", Selected = false},
                 new SelectListItem{Value="Combo Offer 28", Text = "Combo Offer 28", Selected = false},
                 new SelectListItem{Value="Combo Offer 29", Text = "Combo Offer 29", Selected = false},
                 new SelectListItem{Value="Combo Offer 30", Text = "Combo Offer 30", Selected = false},
                 new SelectListItem{Value="Combo Offer 31", Text = "Combo Offer 31", Selected = false},
                 new SelectListItem{Value="Combo Offer 32", Text = "Combo Offer 32", Selected  = false},
                 new SelectListItem{Value="Combo Offer 33", Text = "Combo Offer 33", Selected = false},
                 new SelectListItem{Value="Combo Offer 34", Text = "Combo Offer 34", Selected = false},
                 new SelectListItem{Value="Combo Offer 35", Text = "Combo Offer 35", Selected = false},
                 new SelectListItem{Value="Combo Offer 36", Text = "Combo Offer 36", Selected = false},
                 new SelectListItem{Value="Combo Offer 37", Text = "Combo Offer 37", Selected = false},
                 new SelectListItem{Value="Combo Offer 38", Text = "Combo Offer 39", Selected = false},
                 new SelectListItem{Value="Combo Offer 39", Text = "Combo Offer 39", Selected = false},
                 new SelectListItem{Value="Combo Offer 40", Text = "Combo Offer 40", Selected = false},
                 new SelectListItem{Value="Combo Offer 41", Text = "Combo Offer 41", Selected = false},
                 new SelectListItem{Value="Combo Offer 42", Text = "Combo Offer 42", Selected = false},
                 new SelectListItem{Value="Combo Offer 43", Text = "Combo Offer 43", Selected = false},
                 new SelectListItem{Value="Combo Offer 44", Text = "Combo Offer 44", Selected = false},
                 new SelectListItem{Value="Combo Offer 45", Text = "Combo Offer 45", Selected = false},
                 new SelectListItem{Value="Combo Offer 46", Text = "Combo Offer 46", Selected = false},

                 new SelectListItem{Value="VORTEX 470", Text = "VORTEX 470", Selected = false},
                 new SelectListItem{Value="VORTEX 460", Text = "VORTEX 460", Selected = false},
                 new SelectListItem{Value="VORTEX 590", Text = "VORTEX 590", Selected = false},
                 new SelectListItem{Value="VORTEX 000", Text = "VORTEX 000", Selected = false},
                 new SelectListItem{Value="ZETA PLUS 460", Text = "ZETA PLUS 460", Selected = false},
                 new SelectListItem{Value="ZETA 360", Text = "ZETA 360", Selected = false},
                 new SelectListItem{Value="ZETA 590", Text = "ZETA 590", Selected = false},
                 new SelectListItem{Value="HRC300NF", Text = "HRC300NF", Selected = false},
                 new SelectListItem{Value="HRF305", Text = "HRF305", Selected = false},
                 new SelectListItem{Value="NR300NF", Text = "NR300NF", Selected = false},
                 new SelectListItem{Value="TERESA I-90 PLUS", Text = "TERESA I-90 PLUS", Selected = false},
                 new SelectListItem{Value="IVARAA 60", Text = "IVARAA 60", Selected = false},
                 new SelectListItem{Value="IVARAA 90", Text = "IVARAA 90", Selected = false},
                 new SelectListItem{Value="FRIDA 90", Text = "FRIDA 90", Selected = false},
                 new SelectListItem{Value="FRIDA 75", Text = "FRIDA 75", Selected = false},
                 new SelectListItem{Value="BISCOTTI 60", Text = "BISCOTTI 60", Selected = false},
                 new SelectListItem{Value="BISCOTTI 80", Text = "BISCOTTI 80", Selected = false},
                 new SelectListItem{Value="DATURA PLUS 90", Text = "DATURA PLUS 90", Selected = false},
                 new SelectListItem{Value="REGEN 90", Text = "REGEN 90", Selected = false},
                 new SelectListItem{Value="ABENS 60", Text = "ABENS 60", Selected = false},
                 new SelectListItem{Value="ABENS 90", Text = "ABENS 90", Selected = false},
                 new SelectListItem{Value="CELENA 90", Text = "CELENA 90", Selected = false},
                 new SelectListItem{Value="VETRA ISOLA 90", Text = "VETRA ISOLA 90", Selected = false},
                 new SelectListItem{Value="DIAMOND 77 MWO", Text = "DIAMOND 77 MWO", Selected = false},
                 new SelectListItem{Value="DIAMOND 50MWO", Text = "DIAMOND 50MWO", Selected = false},
                 new SelectListItem{Value="J34MCST", Text = "J34MCST", Selected = false},
                 new SelectListItem{Value="MARINA 8614WD", Text = "MARINA 8614WD", Selected = false},
                 new SelectListItem{Value="CORAL 086", Text = "CORAL 086", Selected = false},
                 new SelectListItem{Value="MARINA 7012 W", Text = "MARINA 7012 W", Selected = false},
                 new SelectListItem{Value="MARINA 8014 W", Text = "MARINA 8014 W", Selected = false},
                 new SelectListItem{Value="MARINA 6010 W", Text = "MARINA 6010 W", Selected = false},
                 new SelectListItem{Value="MARINA 7512 WD", Text = "MARINA 7512 WD", Selected = false},
                 new SelectListItem{Value="SERENE FI 02", Text = "SERENE FI 02", Selected = false},
                 new SelectListItem{Value="SERENE SI 02", Text = "SERENE SI 02", Selected = false},
                 new SelectListItem{Value="AQUA 13XL", Text = "AQUA 13XL", Selected = false},







            };

            ViewBag.Offername = items;


            // Get Utm Parameters Value
            if (utm_source == null)
            {
                TempData["utm_source"] = "";
            }
            else
            {
                TempData["utm_source"] = utm_source;
            }

            if (utm_source == null)
            {
                TempData["utm_medium"] = "";
            }
            else
            {
                TempData["utm_medium"] = utm_medium;
            }

            if (utm_source == null)
            {
                TempData["utm_content"] = "";
            }
            else
            {
                TempData["utm_content"] = utm_content;
            }

            if (utm_source == null)
            {
                TempData["utm_campaign"] = "";
            }
            else
            {
                TempData["utm_campaign"] = utm_campaign;
            }

            if (utm_source == null)
            {
                TempData["Device"] = "";
            }
            else
            {
                TempData["Device"] = Device;
            }

            if (utm_source == null)
            {
                TempData["utm_city"] = "";
            }
            else
            {
                TempData["utm_city"] = City;
            }



            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(OffersLeads obj, FormCollection fobj)
        {

            try
            {
                if (ModelState.IsValid)
                {

                    obj.Offers = fobj["hiddenOffers"].ToString();

                    // remove comma from end of string
                    if (obj.Offers.EndsWith(","))
                    {
                        obj.Offers = obj.Offers.Substring(0, obj.Offers.Length - 1);
                    }


                    obj.utm_source = TempData["utm_source"].ToString();
                    obj.utm_medium = TempData["utm_medium"].ToString();
                    obj.utm_content = TempData["utm_content"].ToString();
                    obj.utm_campaign = TempData["utm_campaign"].ToString();
                    obj.Device = TempData["Device"].ToString();
                    obj.utm_city = TempData["utm_city"].ToString();


                    if (obj.Id == 0)
                    {
                        if (SaveOffers(obj))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ThankYou");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Index");
                        }
                    }

                }
                return View("Index", obj);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("Index");
            }


        }


        public bool SaveOffers(OffersLeads obj)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_OffersLeads"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@FullName", obj.FullName);
                cmd.Parameters.AddWithValue("@Email", obj.Email);
                cmd.Parameters.AddWithValue("@MobileNo", obj.MobileNo);
                cmd.Parameters.AddWithValue("@City", obj.City);
                cmd.Parameters.AddWithValue("@Offers", obj.Offers);
                cmd.Parameters.AddWithValue("@utm_source", obj.utm_source);
                cmd.Parameters.AddWithValue("@utm_medium", obj.utm_medium);
                cmd.Parameters.AddWithValue("@utm_content", obj.utm_content);
                cmd.Parameters.AddWithValue("@utm_campaign", obj.utm_campaign);
                cmd.Parameters.AddWithValue("@Device", obj.Device);
                cmd.Parameters.AddWithValue("@utm_city", obj.utm_city);


                if (util.Execute(cmd))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                    sb.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Contact us enquiry </font></strong></td></tr>");
                    sb.AppendFormat("<tr><td width=\"200px\"><strong>FULL NAME : </strong></td><td> " + obj.FullName + " </td></tr>");
                    sb.AppendFormat("<tr> <td><strong>EMAIL ID  : </strong></td> <td> " + obj.Email + " </td></tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>MOBILE NO. : </strong></td> <td> " + obj.MobileNo + " </td> </tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>CITY : </strong></td> <td> " + obj.City + " </td> </tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>OFFERS : </strong></td> <td> " + obj.Offers + " </td> </tr>");

                    sb.Append("</table>");

                    string ToEmailid = ConfigurationManager.AppSettings["ContactUsEmail"].ToString();
                    String[] emailid = new String[] { ToEmailid };

                    util.SendEmail(sb.ToString(), emailid, "Contact US", "", null);

                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PopupOffers(OffersLeads obj, FormCollection fobj)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    obj.Offers = fobj["hiddenOffersPop"].ToString();

                    // remove comma from end of string

                    if (obj.Offers.EndsWith(","))
                    {
                        obj.Offers = obj.Offers.Substring(0, obj.Offers.Length - 1);
                    }


                    obj.utm_source = TempData["utm_source"].ToString();
                    obj.utm_medium = TempData["utm_medium"].ToString();
                    obj.utm_content = TempData["utm_content"].ToString();
                    obj.utm_campaign = TempData["utm_campaign"].ToString();
                    obj.Device = TempData["Device"].ToString();
                    obj.utm_city = TempData["utm_city"].ToString();


                    if (obj.Id == 0)
                    {
                        if (SaveOffers(obj))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ThankYou");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Index");
                        }
                    }

                }
                return View("Index", obj);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("Index");
            }
        }


        public ActionResult ThankYou()
        {
            return View();
        }
    }
}
